@extends('layout.app')

@section('content')
<div class="display-4">Todos</div>
@if(count($todos) > 0)
<div class="col-md-12 col-lg-12">
    <div class="row">
        @foreach($todos as $todo)

        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
            <div class="card mb-2">
                <div class="card-body">
                    <h3><a href="todo/{{$todo->id}}">{{$todo->text}}</a></h3>
                    <span class="badge badge-pill badge-danger">{{$todo->due}}</span>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif

@endsection