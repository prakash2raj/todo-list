<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TodosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
            array(
                'text' => 'Todo One',
                'body' => 'My First Todo',
                'due' => 'Sunday May 12'
            ),
            array(
                'text' => 'Todo Two',
                'body' => 'My Second Todo',
                'due' => 'Monday May 13'
            )
        ]);

    }
}
