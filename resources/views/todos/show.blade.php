@extends('layout.app')

@section('content')
<a href="/" class="btn btn-sm btn-outline-primary">Go Back</a>
<h1><a href="{{$todo->id}}" class="nav-link">{{$todo->text}}</a></h1>
<div class="badge badge-danger">{{$todo->due}}}</div>
<hr>
<p>{{$todo->body}}</p>
<div class="row">
<div class="col-sx-12 col-sm-2 col-md-2 col-lg-2">
<a href="{{$todo->id}}/edit/" class="btn btn-sm btn-outline-primary">Edit</a>
</div>
<div class="col-sx-12 col-sm-4 col-md-4 col-lg-4 text-left">
{!! Form::open(['action' => ['TodosController@destroy', $todo->id], 'method' => 'POST']) !!}
{{ Form::hidden('_method', 'DELETE')}}
{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
{!! Form::close() !!}
</div>
</div>
@endsection