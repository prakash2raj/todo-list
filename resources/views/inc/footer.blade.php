<div id="footer" class="bg-primary mt-5">
    <div class="container">
        <p class="text-center py-3 text-light">Copyright &copy {{date('Y')}} | Developed By <span><a href="https://about.techprakash.com" target="_blank" title="Web Developer">Prakash Raj</a></span> | Powered By <span class="text-warning">Laravel</span></p>
    </div>
</div>