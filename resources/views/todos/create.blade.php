@extends('layout.app')

@section('content')
<a href="/" class="btn btn-sm btn-outline-primary">Go Back</a>

<h1>Create Todo</h1>
{!! Form::open(['action' => 'TodosController@store', 'method' => 'POST']) !!}

<div class="form-group">
    {{Form::label('text', 'Text', ['class' => 'sr-only'])}}
    {{Form::text('text', '', ['class' => 'form-control', 'id' => 'text', 'placeholder' => 'Enter Text'])}}

    @if($errors->has('text'))
    <p class="alert alert-danger">{{$errors->text}}</p>
    @endif
</div>

<div class="form-group">
    {{Form::label('body', 'Body', ['class' => 'sr-only'])}}
    {{Form::textarea('body', '', ['class' => 'form-control', 'id' => 'body', 'placeholder' => 'Enter Body', 'rows' => 4])}}

    @if($errors->has('body'))
    <p class="alert alert-danger">{{$errors->body}}</p>
    @endif
</div>

<div class="form-group">
    {{Form::label('due', 'Due', ['class' => 'sr-only'])}}
    {{Form::text('due', '', ['class' => 'form-control', 'id' => 'due', 'placeholder' => 'Enter Due'])}}

    @if($errors->has('due'))
    <p class="alert alert-danger">{{$errors->due}}</p>
    @endif
</div>
<div class="row">
    <div class="col-md-4 col-lg-4">
        {{Form::submit('submit', ['class' => 'btn btn-primary btn-block'])}}
    </div>
</div>
{!! Form::close() !!}

@endsection